const express = require('express')
const path = require('path')
const config = require('./core/config.js')

var app = express()

app.use(express.static(__dirname + '/../dist'))

app.get('*', function(req, res) {
	res.sendFile(path.resolve(__dirname, '../dist/', 'index.html'))
})

app.listen(config.port, config.ip, function () {
	console.log('🌎  App running on http://' + config.ip + ':' + config.port)
})