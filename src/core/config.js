module.exports = {

	ip: process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',

	port: process.env.OPENSHIFT_NODEJS_PORT || 4000,

	apiUrl: process.env.NODE_ENV == 'development' ? 'http://localhost:3000' : 'http://wfxapi-nikdo.rhcloud.com/'

};