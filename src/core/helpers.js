var getSpotIconUrl = function(spot) {

	var status = spot.verified ? 'verified' : 'unverified';

	var sportsShortcut = '';
	spot.sports.sort();
	spot.sports.forEach(function(sport) {
		sportsShortcut = sportsShortcut + sport[0];
	});

	return {
		url: '/images/' + sportsShortcut + '-' + status + '.png',
		anchor:  spot.verified ? new google.maps.Point(12, 12) : new google.maps.Point(8, 8)
	};
	
};


// global variable holding preloaded images to save them from garbage collector
// http://stackoverflow.com/q/3646036
window.preloadedImages = [];


var helpers = {

	createSpotMarker: function(spot, map) {
		return new google.maps.Marker({
			map: map,
			title: spot.name,
			position: {lat: spot.lat, lng: spot.lng},
			icon: getSpotIconUrl(spot),
			zIndex: spot.verified ? 2 : 1
		});
	},

	destroyMap: function(map, markers, events) {

		events.map(function(e) {
			google.maps.event.clearListeners(map, e);
		});

		for (var key in markers) {
			markers[key].setMap(null);
			delete markers[key];
		}
		
	},

	preloadImage: function(url) {
		var img = new Image();
		img.src = url;
		window.preloadedImages.push((img));
	},

	getPageTitle: function(pageName) {
		const SERVER_NAME = 'Wind Freaks';
		const TITLE_SEPARATOR = ' - '
		return pageName ? pageName + TITLE_SEPARATOR + SERVER_NAME : SERVER_NAME;
	},

	getGradientColor: function(fade, startColor, endColor) {

		var diff = {
			R: endColor.R - startColor.R,
			G: endColor.G - startColor.G,
			B: endColor.B - startColor.B
		}

		var color = {
			R: Math.round((diff.R * fade) + startColor.R),
			G: Math.round((diff.G * fade) + startColor.G),
			B: Math.round((diff.B * fade) + startColor.B)
		}

		return 'rgb(' + color.R + ',' + color.G +',' + color.B + ')';

	}

};

module.exports = helpers;