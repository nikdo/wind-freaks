import { combineReducers } from 'redux'
import { SPOTS_LOAD_SUCCEEDED, MAP_SAVE_SETTINGS, MAP_RESET_SETTINGS } from './actions'

function spots(state = [], action) {
	switch (action.type) {
		case SPOTS_LOAD_SUCCEEDED:
			return action.data
			break
		default:
			return state
	}
}

const czCenter = { lat: 49.823326, lng: 15.6462 }
const defaultMapSettings = {
	zoom: 7,
	center: czCenter
}

function mapSettings(state = defaultMapSettings, action) {
	switch (action.type) {
		case MAP_SAVE_SETTINGS:
			return action.settings
		case MAP_RESET_SETTINGS:
			return defaultMapSettings
		default:
			return state
	}
}

const rootReducer = combineReducers({
	spots,
	mapSettings
})

export default rootReducer