import React from 'react'
import DocumentTitle from 'react-document-title'
import Helpers from '../core/helpers'

function getDisplayName(name, region) {
	return region ? region + ' / ' + name : name
}

const SpotName = ({ name, region, verified }) => (
	<DocumentTitle title={Helpers.getPageTitle(getDisplayName(name, region))}>
		<h2>
			{getDisplayName(name, region)}
			{verified ? '' : <span className="unverified"> (unverified)</span>}
		</h2>
	</DocumentTitle>
)

SpotName.propTypes = {
	name: React.PropTypes.string.isRequired,
	region: React.PropTypes.string,
	verified: React.PropTypes.bool.isRequired
}

export default SpotName