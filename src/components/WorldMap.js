import React, { PropTypes } from 'react'
import { History } from 'react-router'
import Helpers from '../core/helpers'

var saveMapSettingsTimeoutID = null
var state = {
	map: null,
	markers: {},
	selectedSpot: null
}

export default React.createClass({

	propTypes: {
		mapSettings: PropTypes.shape({
			center: PropTypes.any.isRequired,
			zoom: PropTypes.any.isRequired
		}).isRequired,
		saveMap: PropTypes.func.isRequired,
		spots: PropTypes.arrayOf(PropTypes.shape({
			_id: PropTypes.string.isRequired,
			name: PropTypes.string.isRequired,
			lat: PropTypes.number.isRequired,
			lng: PropTypes.number.isRequired,
			verified: PropTypes.bool.isRequired,
			sports: PropTypes.arrayOf(PropTypes.string).isRequired
		})).isRequired,
		selectedSpot: PropTypes.shape({
			_id: PropTypes.string.isRequired
		})
	},

	mixins: [History],

	mapClicked: function() {
		this.history.pushState(null, '/');
	},

	storeMapSettings: function() {
		var map = state.map;
		var saveMap = this.props.saveMap
		window.clearTimeout(this.saveMapSettingsTimeoutID);
		this.saveMapSettingsTimeoutID = window.setTimeout(function() {
			saveMap({
				zoom: map.getZoom(),
				center: map.getCenter()
			});
		}, 500)
		
	},

	spotClicked: function(id) {
		this.history.pushState(null, `/teaser/${id}`);
	},

	createMap: function() {

		var mapOptions = {
			center: this.props.mapSettings.center,
			zoom: this.props.mapSettings.zoom,
			mapTypeId: google.maps.MapTypeId.TERRAIN,
			mapTypeControl: false,
			streetViewControl: false,
			zoomControlOptions: {
				position: google.maps.ControlPosition.RIGHT_BOTTOM
			}
		};

		state.map = new google.maps.Map(this.refs.map, mapOptions);
		google.maps.event.addListener(state.map, 'click', this.mapClicked);
		google.maps.event.addListener(state.map, 'zoom_changed', this.storeMapSettings);
		google.maps.event.addListener(state.map, 'center_changed', this.storeMapSettings);

	},

	createMarker: function(spot) {
		var marker = Helpers.createSpotMarker(spot, state.map);
		google.maps.event.addListener(marker, 'click', this.spotClicked.bind(this, spot._id));
		state.markers[spot._id] = marker;
	},

	selectSpot: function(spot) {

		// deselect previously selected spot marker
		if (state.selectedSpot)
			state.markers[state.selectedSpot._id].setAnimation(null)	

		// if there is a spot to activate
		if (spot)
		{
			// function to be called asynchronously if map bounds not loaded
			var panToMarker = function(marker) {
				if (!this.getBounds().contains(marker.getPosition()))
					this.panTo(marker.getPosition());
			};

			var marker = state.markers[spot._id];
			if (state.map.getBounds())
				panToMarker.call(state.map, marker);
			else
			{
				var listener = google.maps.event.addListener(state.map, 'bounds_changed', function() {
					panToMarker.call(this, marker);
					google.maps.event.removeListener(listener);
				});
			}
			marker.setAnimation(google.maps.Animation.BOUNCE)

		}
		state.selectedSpot = spot;
	},

	updateMapState: function() {

		const props = this.props;

		if (Object.keys(state.markers).length === 0 && props.spots.length > 0)
		{
			props.spots.map(function(spot) {
				this.createMarker(spot);
			}, this);
		}

		if (props.mapSettings.center != state.map.getCenter())
			state.map.panTo(props.mapSettings.center);
		
		if (props.mapSettings.zoom != state.map.getZoom())
			state.map.setZoom(props.mapSettings.zoom);

		// this has to be executed after center change, because it starts panning to spot
		if (props.selectedSpot != state.selectedSpot)
			this.selectSpot(props.selectedSpot);

	},

	componentDidMount: function() {
		this.createMap();
		this.updateMapState();
	},

	componentDidUpdate: function() {
		this.updateMapState();
	},

	componentWillUnmount: function() {

		// I have to destroy map, all markers and registered events, otherwise it will
		// cause toubles.
		// http://stackoverflow.com/questions/10485582/what-is-the-proper-way-to-destroy-a-map-instance
		const registeredEvents = ['click', 'zoom_changed', 'center_changed'];
		Helpers.destroyMap(state.map, state.markers, registeredEvents);

		// I have to reset it this way, React somehow reinitializes all instance variables
		// during next mount except if variables are stored in an object-typed variable
		state.map = null;
		state.markers = {};
		state.selectedSpot = null;
		
	},

	render: function() {
		return (
			<div className="map" ref="map" />
		);
	}

})
