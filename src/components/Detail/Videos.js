
import React from 'react'

const Videos = ({ videos }) => (
	<section>
		<h3>Videos</h3>
		<ul>
			{videos.map(function(link) {
				return (
					<li key={link.url}>
						<a href={link.url} target="_blank">{link.title}</a>
					</li>
				)
			})}
		</ul>
	</section>
)

Videos.propTypes = {
	videos: React.PropTypes.arrayOf(React.PropTypes.shape({
		url: React.PropTypes.string.isRequired,
		title: React.PropTypes.string.isRequired
	})).isRequired
}

export default Videos