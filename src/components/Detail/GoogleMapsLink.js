import React from 'react'

const GoogleMapLink = ({ lat, lng }) => {

	const linkFormat = 'http://www.google.com/maps/place/<lat>,<lng>/@<lat>,<lng>,<zoom>z/data=!3m1!1e3';
	const zoom = 15;
	const link = linkFormat
		.replace(/<lat>/g, lat)
		.replace(/<lng>/g, lng)
		.replace(/<zoom>/g, zoom)

	return (
		<section id="google-maps-link">
			<a href={link} target="_blank">
				Open in Google Maps
			</a>
		</section>
	)

}

GoogleMapLink.propTypes = {
	lat: React.PropTypes.number.isRequired,
	lng: React.PropTypes.number.isRequired
}

export default GoogleMapLink