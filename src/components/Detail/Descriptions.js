import React  from 'react'

const Descriptions = ({ descriptions }) => (
	<section>
		<h3>About</h3>
		<ul>
			{descriptions.map(function(desc) {
				return (
					<li key={desc.url}>
						<a href={desc.url} target="_blank">{desc.website}</a>
					</li>
				)
			})}
		</ul>
	</section>
)

Descriptions.propTypes = {
	descriptions: React.PropTypes.arrayOf(React.PropTypes.shape({
		url: React.PropTypes.string.isRequired,
		website: React.PropTypes.string.isRequired
	})).isRequired
}

export default Descriptions
