import React from 'react'
import _ from 'underscore'

const urlPatterns = {
	Windguru: 'http://www.windguru.cz/int/index.php?sc=',
	Windfinder: 'http://www.windfinder.com/forecast/'
};

const Forecast = ({ forecasts }) => (
	<section id="forecast">
		<h3 className="visuallyhidden">Forecast</h3>
		<ul>
			{_.map(forecasts, (forecastId, serverName) => (
				<li key={serverName} className={serverName.toLowerCase() + '-link'}>
					<a href={urlPatterns[serverName] + forecastId} target="_blank">
						{serverName}
					</a>
				</li>
			))}
		</ul>
	</section>
)

Forecast.propTypes = {
	forecasts: React.PropTypes.shape({
		Windguru: React.PropTypes.string,
		Windfinder: React.PropTypes.string
	}).isRequired
}

export default Forecast
