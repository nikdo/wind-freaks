import React from 'react'
import Helpers from '../../core/helpers'

var map = null;
var marker = null;
var area = null;
var parking = null;

function initializeMap(center, DOMNode) {

	var mapOptions = {
		center: center,
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		mapTypeControl: true,
		streetViewControl: true,
		zoomControlOptions: {
			position: google.maps.ControlPosition.RIGHT_BOTTOM
		}
	};

	return new google.maps.Map(DOMNode, mapOptions);

};

function renderArea(geoJson, map) {
	var path = geoJson.coordinates[0].map(function(coord) {
		return { lat: coord[1], lng: coord[0] };
	});
	path.pop(); // remove last vertice = repeated first vertice in GeoJSON
	return new google.maps.Polygon({
		map: map,
		paths: path,
		clickable: false,
		fillColor: '#c728a0',
		fillOpacity: 0.2,
		strokeColor: '#c728a0',
		strokeWeight: 2
	});
};

function renderParking(geoJson, map) {
	geoJson.coordinates.forEach(function(coord) {
		return new google.maps.Marker({
			map: map,
			title: 'Parking',
			clickable: false,
			position: { lat: coord[1], lng: coord[0] },
			icon: {
				url: '/images/markers.png',
				size: new google.maps.Size(30, 34),
				origin: new google.maps.Point(0, 0)
			}
		});
	});
};


export default React.createClass({

	propTypes: {
		spot: React.PropTypes.shape({
			name: React.PropTypes.string.isRequired,
			lat: React.PropTypes.number.isRequired,
			lng: React.PropTypes.number.isRequired,
			verified: React.PropTypes.bool.isRequired,
			area: React.PropTypes.object,
			parking: React.PropTypes.object
		})
	},

	updateMap: function() {

		const { spot } = this.props

		if (spot)
		{
			if (!map)
				map = initializeMap(
					{ lat: spot.lat, lng: spot.lng },
					this.refs.map
				)
			if (!marker)
				marker = Helpers.createSpotMarker(spot, map);
			if (!area && spot.area) {
				area = renderArea(spot.area, map);
			}
			if (!parking && spot.parking)
				parking = renderParking(spot.parking, map)
		}

	},

	componentDidMount: function() {
		this.updateMap();
	},

	componentDidUpdate: function() {
		this.updateMap();
	},

	componentWillUnmount: function() {
		map = null;
		marker = null;
		area = null;
		parking = null;
	},

	render: function() {
		return (
			<div className="spotmap" ref="map" />
		);
	}

})
