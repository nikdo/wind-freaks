import React from 'react'
import { Link } from 'react-router'
import SpotName from '../SpotName'
import Sports from '../Sports'
import SpotMap from './Map'
import Elevation from './Elevation'
import Forecast from './Forecast'
import GoogleMapsLink from './GoogleMapsLink'
import Descriptions from './Descriptions'
import Videos from './Videos'
import Links from './Links'

const Detail = ({ spot }) => (
	!spot ? <div></div> : (
		<div>
			<SpotMap spot={spot} />
			<section id="description" className={spot.verified ? '' : 'unverified'}>
				<Link to={`/teaser/${spot._id}`}
					className="close ir"
					title="Close">
						Close
				</Link>
				<SpotName name={spot.name} region={spot.region} verified={spot.verified} />
				<Sports sports={spot.sports} />
				{spot.elevation &&
					<Elevation elevation={spot.elevation} sports={spot.sports} />}
				{spot.forecasts &&
					<Forecast forecasts={spot.forecasts} />}
				<GoogleMapsLink lat={spot.lat} lng={spot.lng} />
				{spot.descriptions.length > 0 &&
					<Descriptions descriptions={spot.descriptions} />}
				{spot.videos.length > 0 &&
					<Videos videos={spot.videos} />}
				{spot.links.length > 0 &&
					<Links links={spot.links} />}
			</section>
		</div>
	)
)

Detail.propTypes = {
		spot: React.PropTypes.shape({
			_id: React.PropTypes.string.isRequired,
			verified: React.PropTypes.bool.isRequired
		})
}

export default Detail
