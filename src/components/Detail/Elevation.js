import React from 'react'

const Elevation = ({ elevation, sports }) => (
	<section id="elevation" className={sports.indexOf('snowkiting') != -1 ? 'snow' : 'land'}>
		<h3 className="visuallyhidden">Elevation</h3>
		<span>{elevation}m</span>
	</section>
)

Elevation.propTypes = {
	elevation: React.PropTypes.number.isRequired,
	sports: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
}

export default Elevation
