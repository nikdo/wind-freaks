import React from 'react'

const Links = ({ links }) => (
	<section id="links">
		<h3>Links</h3>
		<ul>
			{links.map(function(link) {
				return (
					<li key={link.url}>
						<a href={link.url} target="_blank">{
							link.website + ': ' + link.title
						}</a>
					</li>
				)
			})}
		</ul>
	</section>
)

Links.propTypes = {
	links: React.PropTypes.arrayOf(React.PropTypes.shape({
		url: React.PropTypes.string.isRequired,
		title: React.PropTypes.string.isRequired,
		website: React.PropTypes.string.isRequired
	})).isRequired
}

export default Links
