import React from 'react'
import _  from 'underscore'
import { History } from 'react-router'
import Autosuggest from'react-autosuggest'
import { remove as removeDiacritics } from 'diacritics'
import SearchSuggestion from './SearchSuggestion'

export default React.createClass({

	propTypes: {
		spots: React.PropTypes.arrayOf(React.PropTypes.shape({
				_id: React.PropTypes.string.isRequired,
				name: React.PropTypes.string.isRequired,
				region: React.PropTypes.string
			})).isRequired,
		hideSearch: React.PropTypes.func.isRequired
	},

	mixins: [History],

	getInitialState: function() {
		return {
			searchValue: '',
			suggestions: this.getSuggestions('')
		}
	},

	inputChange: function(event, { newValue, method }) {
		if (method === 'type') {
			newValue = newValue.charAt(0).toUpperCase() + newValue.substr(1)
			this.setState({
				searchValue: newValue,
				suggestions: this.getSuggestions(newValue)
			})
		}
	},

	getSearchRegExp: function(input) {
		return new RegExp('^' + removeDiacritics(input), 'i')
	},

	getSuggestions: function(value) {
		const searchRegex = this.getSearchRegExp(value);
		return _(this.props.spots).chain()
				.filter((spot) =>
					searchRegex.test(removeDiacritics(spot.name)) ||
					searchRegex.test(removeDiacritics(spot.region ? spot.region : '')))
				.sortBy('name')
				.sortBy('region')
				.value()
	},

	renderSuggestion: function(spot, { value }) {
		return <SearchSuggestion
			regex={this.getSearchRegExp(value)}
			region={spot.region}
			name={spot.name} />
	},

	getSuggestionValue: function(spot) {
		return spot.region
			? spot.region + ' / ' + spot.name
			: spot.name
	},

	onSuggestionSelected: function(event, { suggestion }) {
		this.setState(this.getInitialState())
		this.props.hideSearch()
		this.history.pushState(null, `/teaser/${suggestion._id}`)
	},

	render: function() {

		const { searchValue, suggestions } = this.state
		const inputProps = {
			id: 'search',
			placeholder: 'Where are you going?',
			value: searchValue,
			spellCheck: false,
			onChange: this.inputChange,
			onBlur: this.props.hideSearch,
			ref: (input) => input && input.focus()
		}

		return (
			<div className="search">
				<label htmlFor="search">Search: </label>
				<Autosuggest
					suggestions={suggestions}
					getSuggestionValue={this.getSuggestionValue}
					renderSuggestion={this.renderSuggestion}
					onSuggestionSelected={this.onSuggestionSelected}
					focusInputOnSuggestionClick={false}
					inputProps={inputProps}/>
			</div>
		)
	}
})
