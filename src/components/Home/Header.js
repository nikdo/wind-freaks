import React from 'react'
import { Link, State } from 'react-router'
import Helpers from '../../core/helpers'
import Search from './Search'

export default React.createClass({

	mixins: [State],

	propTypes: {
		resetMap: React.PropTypes.func.isRequired
	},

	getInitialState: function() {
		return {
			logoHover: false,
			searchActive: false
		}
	},

	componentDidMount: function() {
		Helpers.preloadImage('/images/freaks-hi.gif');
	},

	showSearch: function() {
		this.setState({ searchActive: true });
	},

	hideSearch: function() {
		this.setState({ searchActive: false });
	},

	render: function() {

		var logoImage = '/images/' + (this.state.logoHover && !Modernizr.touch ? 'freaks-hi.gif' : 'freaks.gif');

		return (
			<header className={ this.state.searchActive ? 'search-active' : '' }>
				<Link to="/"
					onClick={this.props.resetMap}
					onMouseOver={ () => this.setState({ logoHover: true }) }
					onMouseOut={ () => this.setState({ logoHover: false }) }
					id="logo"
					title="Return to homepage">
						<img src={ logoImage } alt="Wind Freaks logo" />
				</Link>
				<h1 className="wfx">
					<span className="wind">Wind</span>&nbsp;<span className="freaks">Freaks</span>
				</h1>
				<nav>
					<a href="javascript:void(0)" className="search-link" onClick={this.showSearch}>
						Find Spot
					</a>
					<Link to="/why">
						Why?
					</Link>
				</nav>
				<Search
					searchActive={this.state.searchActive}
					hideSearch={this.hideSearch}
					spots={this.props.spots} />
			</header>
		);
	}
})
