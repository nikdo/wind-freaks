import React from 'react'
import WorldMap from '../WorldMap'
import Header from './Header'

export default ({ spots, resetMap, selectedSpot, mapSettings, saveMap, children }) => (
	<div>
		<Header
			spots={spots}
			resetMap={resetMap} />
		<WorldMap
			spots={spots}
			selectedSpot={selectedSpot}
			mapSettings={mapSettings}
			saveMap={saveMap} />
		{children}
	</div>
)