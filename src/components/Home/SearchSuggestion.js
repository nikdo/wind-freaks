import React from 'react'
import { remove as removeDiacritics } from 'diacritics'

function getHighlightedMarkup(text, regex) {

	// Highlighting after removing diacritics won't work very well because single letter can
	// by replaced by multiple and string lengths won't match. But these are rare cases.
	var result = regex.exec(removeDiacritics(text))
	var start = result ? result.index : 0
	var end = result ? result.index + result[0].length : 0

	return (
		<span>
			<span>{text.slice(0, start)}</span>
			<strong>{text.slice(start, end)}</strong>
			<span>{text.slice(end, text.length)}</span>
		</span>
	)
}

function getRegionMarkup(region, regex) {
	return region
		? <span>{getHighlightedMarkup(region, regex)} / </span>
		: null
}

function getNameMarkup(name, regex) {
	return getHighlightedMarkup(name, regex)
}

const SearchSuggestion = ({ region, name, regex }) => (
	<span>
		{getRegionMarkup(region, regex)}
		{getNameMarkup(name, regex)}
	</span>
)

SearchSuggestion.propTypes = {
	name: React.PropTypes.string.isRequired,
	region: React.PropTypes.string,
	regex: React.PropTypes.object.isRequired
}

export default SearchSuggestion
