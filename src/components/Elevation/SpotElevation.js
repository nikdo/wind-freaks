import React from 'react'
import { Link } from 'react-router'
import Helpers from '../../core/helpers'

function getColor(percentFade) {
	const startColor = { R: 200, G: 234, B: 224 };
	const endColor = { R: 217, G: 240, B: 255 };
	return Helpers.getGradientColor(percentFade, startColor, endColor)
}

function getBarWidth(value, minValue, maxValue) {
	return (value - minValue) / (maxValue - minValue); // nubmer in interval 0 - 1
}

const SpotElevation = ({ spot, minElevation, maxElevation }) => {

	const barFraction = getBarWidth(spot.elevation, minElevation, maxElevation)
	const color = getColor(barFraction)

	return (
		<tr>
			<td className="label">
				<Link to={`/spot/${spot._id}`} target="_blank">
					<span className="region">{spot.region ? spot.region + ' / ' : '' }</span><span>{spot.name}</span>
				</Link>
			</td>
			<td className="elevation">
				<span style={{backgroundColor: color}}>{spot.elevation}m</span>
			</td>
			<td className="bar">
				<span className="fill" style={{width: (barFraction * 100) + '%', backgroundColor: color}}>&nbsp;</span>
				<span className="bg">&nbsp;</span>
			</td>
		</tr>
	)
}

SpotElevation.propTypes = {
	spot: React.PropTypes.shape({
		_id: React.PropTypes.string.isRequired,
		elevation: React.PropTypes.number.isRequired,
		name: React.PropTypes.string.isRequired,
		region: React.PropTypes.string
	}),
	minElevation: React.PropTypes.number.isRequired,
	maxElevation: React.PropTypes.number.isRequired
}

export default SpotElevation
