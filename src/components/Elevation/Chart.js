import React, { Component } from 'react'
import _ from 'underscore'
import Loader from '../Loader'
import SpotElevation from './SpotElevation'

function sortSpots(spots, groupRegions) {

	var sorted = _.sortBy(spots, function (spot) {
		return -spot.elevation
	});

	if (groupRegions)
		return _(sorted).chain()
			.groupBy('region')
			.sortBy(function (array) {
				var highestSpot = array[0];
				return highestSpot.region ? -highestSpot.elevation : 0;
			})
			.value();
	else
		return [sorted];

}

class Chart extends Component {

	constructor(props) {
		super(props)
		this.state = { groupRegions: true }
	}

	render() {

		var chart;
		if (!this.props.spots.length) {
			chart = <Loader />
		}
		else {

			var maxElevation = _.max(this.props.spots, 'elevation').elevation;
			var minElevation = _.min(this.props.spots, 'elevation').elevation;
			var groups = sortSpots(this.props.spots, this.state.groupRegions);

			chart =
				<table className="chart">

					{groups.map(function (spots) {
						return (
							<tbody key={spots[0].region || 'No Region'}>

								{spots.map(function(spot) {
									// random key to rerender all bars on change -> animation
									return <SpotElevation
										key={Math.random()}
										spot={spot}
										minElevation={minElevation}
										maxElevation={maxElevation} />
								})}

							</tbody>
						)
					})}

				</table>

		}

		return (
			<div>

				<form>
					<label htmlFor="sort">Seřadit podle: </label>
					<select
						id="sort"
						value={this.state.groupRegions ? 'region' : 'elevation'}
						onChange={ (event) => this.setState({groupRegions: event.target.value == 'region'}) }>
						<option value="region">Regionu</option>
						<option value="elevation">Nadmořské výšky</option>
					</select>
				</form>

				{chart}

			</div>
		)
	}

}

Chart.propTypes = {
	spots: React.PropTypes.arrayOf(React.PropTypes.shape({
		region: React.PropTypes.string,
		elevation: React.PropTypes.number.isRequired
	})).isRequired
}

export default Chart
