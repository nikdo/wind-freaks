import React from 'react'
import DocumentTitle from 'react-document-title'
import { Link } from 'react-router'
import Helpers from '../../core/helpers'
import Chart from './Chart'

export default ({ spots, resetMap }) => (
	<DocumentTitle title={Helpers.getPageTitle('Nadmořská výška')}>
		<div id="on-the-top" className="standalone">
			<article>

				<img src="/images/mountains.png" alt="Mountains partially cowered by snow" />

				<h1>Nadmořská výška</h1>

				<p>Snowkiting v Česku? Čím výš, tím líp.</p>

				<Chart spots={spots} />

				<p>
					<a href="http://portal.chmi.cz/files/portal/docs/poboc/OS/OMK/mapy/prohlizec.html?map=SCE" target="_blank">
						ČHMÚ: Aktuální sněhová pokrývka
					</a>
				</p>

				<p>
					Powered by <Link to="/" className="wfx" onClick={resetMap}>
						<span className="wind">Wind</span>&nbsp;<span className="freaks">Freaks</span>
					</Link>.
				</p>

			</article>
		</div>
	</DocumentTitle>
)
