import React from 'react'

const Sports = ({ sports }) => (
	<section id="sports">
		<h3 className="visuallyhidden">Sports</h3>
		<ul>
			{sports.map((sport) => (<li key={sport} className={sport}>{sport}</li>))}
		</ul>
	</section>
)

Sports.propTypes = {
	sports: React.PropTypes.arrayOf(React.PropTypes.string).isRequired
}

export default Sports