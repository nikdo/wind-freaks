import React from 'react'
import { Link } from 'react-router'
import DocumentTitle from 'react-document-title'
import Helpers from '../../core/helpers'

const Why = ({ spotsCount }) => (
	<DocumentTitle title={Helpers.getPageTitle('Why')}>
		<div id="why" className="standalone">
			<nav>
				<Link to="/" className="close ir" title="Close">
					Close
				</Link>
			</nav>

			<article>

				<img src="/images/freaks-all.gif" alt="Wind Freaks logo" />

				<h1 className="wfx">We are <span className="wind">Wind</span>&nbsp;<span className="freaks">Freaks</span>.</h1>

				<p>Adrenaline flows in our veins as we are blasting at full speed, powered by the force of nature.</p>

				<picture>
					<source media="(min-width: 582px)"
						srcSet="/images/watersports-wide.png 550w,
							/images/watersports-wide-dd.png 1100w"
						sizes="550px" />
					<source media="(min-width: 482px)"
						srcSet="/images/watersports-medium.png 450w,
							/images/watersports-medium-dd.png 900w"
						sizes="450px" />
					<source
						srcSet="/images/watersports-small.png 280w,
							/images/watersports-small-dd.png 560w"
						sizes="280px" />
					<img srcSet="/images/watersports-small.png" alt="Windsurfer and kiteboarder on a sea" />
				</picture>

				<p>Kiting and windsurfing are sports like no other.</p>

				<p>There are many wonderful places on this planet. But we have spent too much time staring at computer screens searching for these places.</p>

				<img
					srcSet="/images/earth-map.png 240w,
						/images/earth-map-dd.png 480w"
					sizes="240px"
					alt="Earth with map markers surrounded by people" />

				<p>So we map kiting and windsurfing locations for people like us.</p>

				<figure className="infographic">
					<dl className="number">
						<dt>{spotsCount}</dt>
						<dd>Spots discovered by now</dd>
					</dl>
				</figure>

				<p>Existing applications are chaotic, they brim with advertisements and lack the necessary information. We aim to provide both quality and quantity in a way that is easy to comprehend.</p>

				<p className="buttons">
					<Link to="/">Find your dream spot.</Link>
				</p>

				<p className="social-links">
					<a href="https://twitter.com/wind_freaks"
						className="twitter ir"
						title="Twitter"
						target="_blank">
							Twitter
					</a>
					<a href="https://www.facebook.com/windfreaksnet/"
						className="facebook ir"
						title="Facebook"
						target="_blank">
							Facebook
					</a>
					<a href="mailto:windfreaks@nikdo.cz"
						className="email ir"
						title="Email">
							Email
					</a>
				</p>

			</article>
		</div>
	</DocumentTitle>
)

Why.propTypes = {
	spotsCount: React.PropTypes.number.isRequired
}

export default Why
