import React from 'react'
import { Link } from 'react-router'
import SpotName from '../SpotName'
import Sports from '../Sports'

const Teaser = ({ spot }) => (
	!spot ? <section id="teaser"></section> : (
		<Link to={`/spot/${spot._id}`} title="Explore">
			<section id="teaser" className={spot.verified ? '' : 'unverified'}>
				<SpotName name={spot.name} region={spot.region} verified={spot.verified} />
				<Sports sports={spot.sports} />
				{spot.linksCount > 0 &&
					<section>
						<strong>{spot.linksCount}</strong> link{spot.linksCount != 1 && 's'}
					</section>
				}
			</section>
		</Link>
	)
)

Teaser.propTypes = {
	spot: React.PropTypes.shape({
		_id: React.PropTypes.string.isRequired,
		verified: React.PropTypes.bool.isRequired,
		sports: React.PropTypes.any.isRequired,
		linksCount: React.PropTypes.number.isRequired
	}),
}

export default Teaser
