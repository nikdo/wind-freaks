import config from './core/config'
import fetch from 'isomorphic-fetch'

export const SPOTS_LOAD = 'SPOTS_LOAD'
export const SPOTS_LOAD_SUCCEEDED = 'SPOTS_LOAD_SUCCEEDED'
export const MAP_SAVE_SETTINGS = 'MAP_SAVE_SETTINGS'
export const MAP_RESET_SETTINGS = 'MAP_RESET_SETTINGS'

export function requestSpots() {
	return {
		type: SPOTS_LOAD
	}
}

export function receiveSpots(json) {
	return {
		type: SPOTS_LOAD_SUCCEEDED,
		data: json
	}
}

export function fetchSpots() {
	return dispatch => {
		dispatch(requestSpots())
		fetch(config.apiUrl)
			.then(response => response.json())
			.then(json => dispatch(receiveSpots(json)))
	}
}

export function saveMapSettings(settings) {
	return {
		type: MAP_SAVE_SETTINGS,
		settings: settings
	}
}

export function resetMapSettings() {
	return {
		type: MAP_RESET_SETTINGS
	}
}
