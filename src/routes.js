import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from './containers/App'
import Home from './containers/Home'
import Teaser from './containers/Teaser'
import Detail from './containers/Detail'
import Why from './containers/Why'
import Elevation from './containers/Elevation'

export default
	<Route path="/" component={App}>
		<Route component={Home}>
			<IndexRoute />
			<Route path="teaser/:id" component={Teaser} />
		</Route>
		<Route path="spot/:id" component={Detail} />
		<Route path="why" component={Why} />
		<Route path="elevation" component={Elevation} />
	</Route>