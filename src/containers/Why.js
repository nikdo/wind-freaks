import React, { Component } from 'react'
import { connect } from 'react-redux'
import WhyComponent from '../components/Why/Why'

class Why extends Component {
	render() {
		return (<WhyComponent spotsCount={this.props.spotsCount} />)
	}
}

export default connect(function(state, ownProps) {
	return {
		spotsCount: state.spots.length
	}
})(Why)
