import React, { Component } from 'react'
import { connect } from 'react-redux'
import ElevationComponent from '../components/Elevation/Elevation'
import { resetMapSettings } from '../actions'

class Elevation extends Component {
	render() {
		return (
			<ElevationComponent
				spots={this.props.spots}
				resetMap={() => this.props.dispatch(resetMapSettings())} />
		)
	}
}

export default connect(function(state, ownProps) {
	return {
		spots: state.spots.filter(
			(spot) => spot.country == 'CZ' && spot.sports.indexOf('snowkiting') >= 0
		)
	}
})(Elevation)
