import React, { Component } from 'react'
import { connect } from 'react-redux'
import DetailComponent from '../components/Detail/Detail'

class Detail extends Component {
	render() { return <DetailComponent spot={this.props.spot} /> }
}

export default connect(function(state, ownProps) {
	return {
		spot: state.spots.filter(spot => spot._id == ownProps.params.id)[0]
	}
})(Detail)
