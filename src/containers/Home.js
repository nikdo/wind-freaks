import React, { Component } from 'react'
import { connect } from 'react-redux'
import HomeComponent from '../components/Home/Home'
import { saveMapSettings, resetMapSettings } from '../actions'

class Home extends Component {

	render() {
		const { dispatch, spots, selectedSpot, mapSettings } = this.props
		return (
			<div>
				<HomeComponent
					spots={spots}
					selectedSpot={selectedSpot}
					mapSettings={mapSettings}
					saveMap={(settings) => dispatch(saveMapSettings(settings))}
					resetMap={() => dispatch(resetMapSettings())} />
				{this.props.children}
			</div>
		)
	}
}

export default connect(function(state, ownProps) {
	return {
		spots: state.spots,
		selectedSpot: state.spots.filter(spot => spot._id == ownProps.params.id)[0],
		mapSettings: state.mapSettings
	}
})(Home)
