import React, { Component } from 'react'
import DocumentTitle from 'react-document-title'
import { connect } from 'react-redux'
import Helpers from '../core/helpers'
import { fetchSpots } from '../actions'

class App extends Component {

	componentDidMount() {
		this.props.dispatch(fetchSpots())
	}

	render() {
		return (
			<DocumentTitle title={Helpers.getPageTitle()}>
				{this.props.children}
			</DocumentTitle>
		)
	}

}

export default connect()(App)
