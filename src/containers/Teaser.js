import React, { Component } from 'react'
import { connect } from 'react-redux'
import SpotTeaser from '../components/Teaser/Teaser'

class Teaser extends Component {
	render() { return <SpotTeaser spot={this.props.spot} /> }
}

export default connect(function(state, ownProps) {
	return {
		spot: state.spots
			.filter(spot => spot._id == ownProps.params.id)
			.map(spot => (Object.assign(
				{ linksCount: spot.descriptions.length + spot.videos.length + spot.links.length },
				spot
			)))[0]
	}
})(Teaser)
