# Wind Freaks

Wind Freaks - windsurfing & kiting community page.


## Development Environment

1. Start wind-freaks-api back-end application.

2. Start development tools and application with development settings:
	
	`npm start`


## Deployment Procedure

**Prerequisity:** *NODE_ENV* variable is set to *production*. Can be set permanently by executing following command: `rhc env set NODE_ENV=production -a wfx`



Push changes to OpenShift:

    `git push openshift`

OpenShift builds the aplication with production settings by running *build* script during deployment process. After that OpenShift runs supervisor with script defined in *main* property.

In the case that deployment fails deployment process can be triggered manually:

    `rhc deploy master -a wfx`