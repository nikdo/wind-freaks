# Spots loading

## Spots are loaded
1. open home page
- spots are loaded


# Teaser activation

## Teaser and marker are activated with marker click
1. click on marker
- selected spot teaser is displayed
- selected spot marker is selected
- URL corresponds to teaser

## Teaser and marker are activated with URL
1. inupt teaser URL
- selected spot teaser is displayed
- selected spot marker is selected


# Detail activation


## Detail is activated with teaser click
1. click on marker
2. click on teaser
- spot map and marker are displayed
- URL corresponds to detail

## Detail is activated with URL
1. inupt detail URL
- spot map and marker are displayed


# Pan to marker outside viewport

## Map pans to marker when activated through browser history
1. select a spot
2. move map outside so that spot is not visible
3. select another spot
4. click browser back
- map is centered to first spot

## Map pans to marker when spot activated from URL
1. input URL of spot teaser outside of default viewport
- map is centered to spot marker



# Map reset on home click

## Home click resets map after zoom and center change
1. move map and change zoom
2. click home
- map sholuld be centered on CR_CENTER with zoom 7

## Home click from teaser resets map
1. move map, change zoom and open spot teaser
2. click home
- map sholuld be centered on CR_CENTER with zoom 7


# Map position remembering

## Positon reloads when going back to map
1. move map, change zoom and open spot detail
2. click browser back
- map zoom and position is the same